package Koha::Plugin::Com::RBitTechnology::SmartWithdrawals;

use Modern::Perl;
use base qw(Koha::Plugins::Base);
use Encode qw( decode );
use Text::CSV::Encoded;
use File::Temp;
use File::Basename qw( dirname );
use List::MoreUtils qw/uniq/;
use Mojo::JSON qw(to_json decode_json);
use utf8;
use C4::Auth;
use C4::Biblio;
use C4::Items;
use Koha::AuthorisedValueCategories;
use Koha::AuthorisedValues;
use Koha::DateUtils qw( dt_from_string output_pref );
use Koha::Items;
use Koha::ItemTypes;
use Koha::Libraries;
use Koha::Patrons;
use Koha::Plugin::Com::RBitTechnology::SmartWithdrawals::Trackings;
use Koha::SearchEngine::Indexer;

use Data::Dumper;

our $VERSION = "3.1.3";

our $metadata = {
    name            => 'Inteligentní odpisy a přesuny',
    author          => 'Radek Šiman',
    description     => 'Tento modul poskytuje nástroje pro efektivní vyhledávání jednotek vhodných k odpisu či přesunům.',
    date_authored   => '2017-11-15',
    date_updated    => '2024-09-20',
    minimum_version => '21.11',
    maximum_version => undef,
    version         => $VERSION
};

our $tagslib = &C4::Biblio::GetMarcStructure(1);

our $independentBranches = C4::Context->preference('IndependentBranches') ne '0';

sub new {
    my ( $class, $args ) = @_;

    ## We need to add our metadata here so our base class can access it
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    ## Here, we call the 'new' method for our base class
    ## This runs some additional magic and checking
    ## and returns our actual $self
    my $self = $class->SUPER::new($args);

    return $self;
}

sub install() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table = $self->get_qualified_table_name('predef_options');

    $self->store_data(
        {
            serial_itemtypes => 'PE,PEPREZ,KO',
            permanent_withdrawn_number_items_columns => C4::Context->preference('PermanentWithdrawnNumberItemsColumns') // '',
            permanent_withdrawn_seed => 0,
            start_date => 0
        }
    );
    
    return  C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table_predefs (
            `predef_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(80) NOT NULL,
            `description` TEXT DEFAULT NULL,
            `date_created` DATETIME NOT NULL,
            `last_modified` DATETIME NOT NULL,
            PRIMARY KEY(`predef_id`)
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_czech_ci;
        " ) && C4::Context->dbh->do( "
        CREATE TABLE IF NOT EXISTS $table (
            `predef_option_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
            `predef_id` INT( 11 ) NOT NULL,
            `variable` VARCHAR(50) NOT NULL,
            `value` TEXT NOT NULL,
            PRIMARY KEY(`predef_option_id`),
            INDEX `fk_predef_options_idx` (`predef_id` ASC),
            CONSTRAINT `fk_predef_options`
            FOREIGN KEY (`predef_id`)
            REFERENCES `$table_predefs` (`predef_id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
        ) ENGINE = INNODB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_czech_ci;
    " );
}

sub upgrade {
    my ( $self ) = @_;

    $self->store_data( { serial_itemtypes => 'PE,PEPREZ,KO' } ) if ( !defined $self->retrieve_data('serial_itemtypes') );
    $self->store_data( { permanent_withdrawn_number_items_columns => C4::Context->preference('PermanentWithdrawnNumberItemsColumns') // '' } )  if ( !defined $self->retrieve_data('permanent_withdrawn_number_items_columns') );
    $self->store_data( { permanent_withdrawn_seed => 0 } )      if ( !defined $self->retrieve_data('permanent_withdrawn_seed') );
    $self->store_data( { start_date => 0 } )                    if ( !defined $self->retrieve_data('start_date') );

    return 1;
}

sub uninstall() {
    my ( $self, $args ) = @_;

    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    return C4::Context->dbh->do("DROP TABLE $table_options") && C4::Context->dbh->do("DROP TABLE $table_predefs");
}

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('save') ) {
        my $template = $self->get_template({ file => 'configure.tt' });

        # build categories list
        my @categories = Koha::AuthorisedValueCategories->search({ category_name => { -not_in => ['', 'branches', 'itemtypes', 'cn_source']}}, { order_by => ['category_name'] } );
        my @category_list = ('?');
        for my $category ( @categories ) {
            push( @category_list, $category->category_name );
        }

        ## Grab the values we already have for our settings, if any exist
        $template->param(
            categories                                  => \@category_list,
            ccode                                       => $self->retrieve_data('ccode'),
            acqsource                                   => $self->retrieve_data('acqsource'),
            location                                    => $self->retrieve_data('location'),
            serial_itemtypes                            => $self->retrieve_data('serial_itemtypes'),
            permanent_withdrawn_number_items_columns    => $self->retrieve_data('permanent_withdrawn_number_items_columns'),
            permanent_withdrawn_seed                    => $self->retrieve_data('permanent_withdrawn_seed'),
            start_date                                  => $self->retrieve_data('start_date'),
        );

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');
        print $template->output();
    }
    else {
        $self->store_data(
            {
                ccode                                       => scalar $cgi->param('ccode'),
                acqsource                                   => scalar $cgi->param('acqsource'),
                location                                    => scalar $cgi->param('location'),
                serial_itemtypes                            => scalar $cgi->param('serial_itemtypes'),
                permanent_withdrawn_number_items_columns    => scalar $cgi->param('permanent_withdrawn_number_items_columns'),
                permanent_withdrawn_seed                    => scalar $cgi->param('permanent_withdrawn_seed'),
                start_date                                  => scalar $cgi->param('start_date'),
                last_configured_by                          => C4::Context->userenv->{'number'},
            }
        );

        $self->go_home();
    }
}

sub intranet_js {
    my ( $self ) = @_;
    my $js;
    my $base = $self->bundle_path() . "/js";
    
    if ( $ENV{REQUEST_URI} =~ /\/tools\/tools-home\.pl/ ) {
	    my $tools = slurp_utf8("$base/tools.js");
	
	    $js = "<!-- RBIT SmartWithdrawals/Tools -->\n" .
	        "<script>\n" .
	            $tools . "\n" .
	        "</script>\n";
    }
    elsif ( $ENV{REQUEST_URI} =~ /\/mainpage\.pl/ ) {
        my $mainpage = slurp_utf8("$base/mainpage.js");
    
        $js = "<!-- RBIT SmartWithdrawals/Mainpage -->\n" .
            "<script>\n" .
                $mainpage . "\n" .
            "</script>\n";
    }
    elsif ( $ENV{REQUEST_URI} =~ /\/plugins\/plugins-home\.pl/  ) {
        my $plugins = slurp_utf8("$base/plugins.js");
    
        $js = "<!-- RBIT SmartWithdrawals/Plugins -->\n" .
            "<script>\n" .
                $plugins . "\n" .
            "</script>\n";
    }

    return $js;
}

sub slurp_utf8 {
    my $file = shift;
    my $contents = do {
        open my $fh, '<:encoding(UTF-8)', $file or die "Error reading $file";
        local $/;
        <$fh>;
    };

    return $contents;
}

sub tool {
    my ( $self, $args ) = @_;

    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('phase') ) {;
        $self->tool_list_predefs();
    }
    elsif ( $cgi->param('phase') eq 'edit' ) {
        $self->tool_edit_predef();
    }
    elsif ( $cgi->param('phase') eq 'run' ) {
        $self->tool_get_results();
    }
    elsif ( $cgi->param('phase') eq 'delete' ) {
        $self->tool_delete_predef();
    }
    elsif ( $cgi->param('phase') eq 'duplicate' ) {
        $self->tool_duplicate_predef();
    }
    elsif ( $cgi->param('phase') eq 'export' ) {
        $self->tool_export_results();
    }
    elsif ( $cgi->param('phase') eq 'sql' ) {
        $self->tool_show_sql();
    }
    elsif ( $cgi->param('phase') eq 'withdraw-select' ) {
        $self->tool_withdraw_select();
    }
    elsif ( $cgi->param('phase') eq 'withdraw-show' ) {
        $self->tool_withdraw_show();
    }
    elsif ( $cgi->param('phase') eq 'withdraw-action' ) {
        $self->tool_withdraw_action();
    }
    elsif ( $cgi->param('phase') eq 'ajax-test' ) {
        $self->ajax_test();
    }
}

sub tool_list_predefs {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-list.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');

    my $query = "SELECT predef_id, name, description, date_created, last_modified FROM $table_predefs;";

    my $sth = $dbh->prepare($query);
    $sth->execute();

    my @results;
    while ( my $row = $sth->fetchrow_hashref() ) {
        push( @results, $row );
    }

    $template->param(
        predefs => \@results,
    );

    print $template->output();
}

sub tool_edit_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'tool-edit.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    my $options = {};
    my $predef = undef;
    if ( defined $cgi->param('predef') ) {
        my $dbh = C4::Context->dbh;
        my $table_options = $self->get_qualified_table_name('predef_options');
        my $table_predefs = $self->get_qualified_table_name('predefs');

        my $query = "SELECT predef_id, name, description FROM $table_predefs WHERE predef_id = ?;";
        my $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
        $predef = $sth->fetchrow_hashref();

        $query = "SELECT variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );

        my %multi = map { $_ => 1 } qw(
            acqsource
            branches
            locations
            ccodes
        );

        while ( my $row = $sth->fetchrow_hashref() ) {
            if ( exists( $multi{$row->{variable}} ) ) {
                my @arr = split /,/, $row->{value};
                $options->{$row->{variable}} = \@arr;
            }
            else {
                $options->{$row->{variable}} = $row->{value};
            }
        }
        
	    $options->{chk_onloan} = 0 if ( !defined $options->{chk_onloan} );
	    $options->{onloan} = 0 if ( !defined $options->{onloan} ); 
    }
    
    $options->{chk_onloan} = 1 if ( !defined $options->{chk_onloan} );
    $options->{onloan} = 0 if ( !defined $options->{onloan} ); 

    # prepare item types
    my @itemtypes = Koha::ItemTypes->search({}, { order_by => ['description'], columns => [qw/itemtype description/] } );
    my @branches  = Koha::Libraries->search({}, { order_by => ['branchname'], columns => [qw/branchcode branchname/] } );
    my @locations = Koha::AuthorisedValues->search({ category => $self->retrieve_data('location')},  { order_by => ['lib'], columns => [qw/authorised_value lib/] } );
    my @ccodes    = Koha::AuthorisedValues->search({ category => $self->retrieve_data('ccode')},     { order_by => ['lib'], columns => [qw/authorised_value lib/] } );
    my @acqsource = Koha::AuthorisedValues->search({ category => $self->retrieve_data('acqsource')}, { order_by => ['lib'], columns => [qw/authorised_value lib/] } );
    $template->param(
        itemtypes => \@itemtypes,
        branches => \@branches,
        locations => \@locations,
        ccodes => \@ccodes,
        acqsource => \@acqsource,
        options => $options,
        predef => $predef,
    );

    print $template->output();
}

sub execute_sql {
    my ( $self, $predefId, $returnSql ) = @_;

        # retrieve column list
        my $dbh = C4::Context->dbh;
        my $table_options = $self->get_qualified_table_name('predef_options');
        my $query = "SELECT SUBSTRING(variable, 5) as colname FROM $table_options WHERE variable LIKE 'col_%' AND value = '1' AND predef_id = ?;";
        my $sth = $dbh->prepare($query);
        $sth->execute($predefId);

        my @columns;
        my @queryCols;
        my $titleFormat = 'CONCAT(\'<a href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=\', items.biblionumber, \'" target="_blank">\', title, \'</a>\') as title';
        my $f26x = 'TRIM(ExtractValue(metadata, "//datafield[@tag=\'%d\']/subfield[@code=\'c\']"))';
        my $f260c = sprintf($f26x, 260);
        my $f264c = sprintf($f26x, 264);
        my $publication = "IFNULL(copyrightdate, IF($f260c != '', $f260c, IF($f264c != '', $f264c, ''))) as publicationyear";
        my $callnumber_stock = "ExtractValue(more_subfields_xml, '//datafield[\@tag=\"999\"]/subfield[\@code=\"k\"]') as itemcallnumber_stock";
        while ( my $row = $sth->fetchrow_hashref() ) {
            push( @columns, $row->{colname} );

            push( @queryCols, 'barcode')                                if ( $row->{colname} eq 'barcode' );
            push( @queryCols, $titleFormat)                             if ( $row->{colname} eq 'title' );
            push( @queryCols, 'title as title_text')                    if ( $row->{colname} eq 'title' );
            push( @queryCols, 'author')                                 if ( $row->{colname} eq 'author' );
            push( @queryCols, 'itemtypes.description as itemtype')      if ( $row->{colname} eq 'author' );
            push( @queryCols, 'itemcallnumber')                         if ( $row->{colname} eq 'callnumber' );
            push( @queryCols, $callnumber_stock)                        if ( $row->{colname} eq 'callnumber_stock' );
            push( @queryCols, 'stocknumber')                            if ( $row->{colname} eq 'stocknumber' );
            push( @queryCols, 'branches.branchname as holdingbranch')   if ( $row->{colname} eq 'holdingbranch' );
            push( @queryCols, 'replacementprice')                       if ( $row->{colname} eq 'replacementprice' );
            push( @queryCols, $publication)                             if ( $row->{colname} eq 'publicationyear' );
        }

        # detect enabled subconditions
        my $enabled = {};
        $query = "SELECT SUBSTRING(variable, 5) as subcond FROM $table_options WHERE variable LIKE 'chk_%' AND value = '1' AND predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute($predefId);
        while ( my $row = $sth->fetchrow_hashref() ) {
            $enabled->{$row->{subcond}} = 1;
        }

        # prepare subconditions
        my $subcond = {};
        $query = "SELECT variable, value FROM $table_options WHERE variable NOT LIKE 'col_%' AND variable NOT LIKE 'chk_%' AND predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute($predefId);
        while ( my $row = $sth->fetchrow_hashref() ) {
            $subcond->{$row->{variable}} = $row->{value};
        }
        my $limit;
        my @where;
        my @bindParams;
        my @havingParts;
        my $period = {
            day => 1,
            month => 30,
            year => 365
        };

        if ( defined $subcond->{limit} ) {
            $limit = (" LIMIT " . $subcond->{limit});
        }

        # SELECT parts must be processed before any WHERE params to keep the right order of bindParams
        if ( $enabled->{available_items} ) {
            push( @havingParts, " pocet >= " . $subcond->{available_items} );
            push( @queryCols, '(select count(itemnumber) from items i where i.biblionumber = biblio.biblionumber) as pocet' );
        }
        if ( $enabled->{issues_max} ) {
            push( @queryCols, '(SELECT count(issue_id) FROM issues i WHERE i.itemnumber = items.itemnumber AND datediff(now(), issuedate) / ? <= ?) AS issues_count' );
            push( @queryCols, '(SELECT count(issue_id) FROM old_issues i WHERE i.itemnumber = items.itemnumber AND datediff(now(), issuedate) / ? <= ?) AS old_issues_count');
            push( @havingParts, 'issues_count + old_issues_count <= ' . $subcond->{issues_max} );
            push( @bindParams, $period->{$subcond->{issues_max_period_type}} );
            push( @bindParams, $subcond->{issues_max_period_length} );
            push( @bindParams, $period->{$subcond->{issues_max_period_type}} );
            push( @bindParams, $subcond->{issues_max_period_length} );
        }
        if ( $enabled->{reserves_max} ) {
            push( @queryCols, '(SELECT count(reserve_id) FROM reserves r WHERE r.biblionumber = items.biblionumber AND datediff(now(), reservedate) / ? <= ?) AS reserves_count' );
            push( @queryCols, '(SELECT count(reserve_id) FROM old_reserves r WHERE r.biblionumber = items.biblionumber AND datediff(now(), reservedate) / ? <= ?) AS old_reserves_count');
            push( @havingParts, 'reserves_count + old_reserves_count <= ' . $subcond->{reserves_max} );
            push( @bindParams, $period->{$subcond->{reserves_max_period_type}} );
            push( @bindParams, $subcond->{reserves_max_period_length} );
        }
        if ( $enabled->{last_reserve} ) {
            push( @queryCols, '(SELECT 1 AS last_reserve FROM reserves r WHERE r.biblionumber = items.biblionumber AND datediff(now(), reservedate) / ? >= ? LIMIT 1) as last_reserve' );
            push( @queryCols, '(SELECT 1 AS last_reserve FROM old_reserves r WHERE r.biblionumber = items.biblionumber AND datediff(now(), reservedate) / ? >= ? LIMIT 1) as last_old_reserve' );
            push( @havingParts, '(last_reserve IS NOT NULL OR last_old_reserve IS NOT NULL)' );
            push( @bindParams, $period->{$subcond->{last_reserve_period_type}} );
            push( @bindParams, $subcond->{last_reserve_period_length} );
            push( @bindParams, $period->{$subcond->{last_reserve_period_type}} );
            push( @bindParams, $subcond->{last_reserve_period_length} );
        }
        # WHERE must be processed after SELECT parts to keep the right order of bindParams
        if ( $enabled->{itype} ) {
            push( @where, "items.itype = ?" );
            push( @bindParams, $subcond->{itype} );
        }
        if ( $enabled->{item_price} ) {
            push( @where, "items.replacementprice <= ?" );
            push( @bindParams, $subcond->{item_price} );
        }
        if ( $enabled->{damage} ) {
            my $op = $subcond->{damage} == 0 ? '=' : '!=';
            push( @where, "items.damaged $op ?" );
            push( @bindParams, 0 );
        }
        if ( $enabled->{onloan} ) {
            push( @where, $subcond->{onloan} == 0 ? "items.onloan IS NULL" : "items.onloan IS NOT NULL" );
        }
        if ( $enabled->{acqsource} ) {
            my @sources = split(',', $subcond->{acqsource});
            my @qMarks;
            foreach my $src ( @sources ) {
                push( @bindParams, $src );
                push( @qMarks, '?' );
            }
            push( @where, "items.booksellerid IN (" . join(',', @qMarks) . ")" );
        }
        if ( $enabled->{branches} ) {
            my @branches = split(',', $subcond->{branches});
            my @qMarks;
            foreach my $branch ( @branches ) {
                push( @bindParams, $branch );
                push( @qMarks, '?' );
            }
            push( @where, "items.holdingbranch IN (" . join(',', @qMarks) . ")" );
        }
        if ( $enabled->{locations} ) {
            my @locations = split(',', $subcond->{locations});
            my @qMarks;
            foreach my $loc ( @locations ) {
                push( @bindParams, $loc );
                push( @qMarks, '?' );
            }
            push( @where, "items.location IN (" . join(',', @qMarks) . ")" );
        }
        if ( $enabled->{ccodes} ) {
            my @ccodes = split(',', $subcond->{ccodes});
            my @qMarks;
            foreach my $ccode ( @ccodes ) {
                push( @bindParams, $ccode );
                push( @qMarks, '?' );
            }
            push( @where, "items.ccode IN (" . join(',', @qMarks) . ")" );
        }
        if ( $enabled->{last_issue} ) {
            push( @where, "DATEDIFF(now(), items.datelastborrowed) / ? >= ?" );
            push( @bindParams, $period->{$subcond->{last_issue_period_type}} );
            push( @bindParams, $subcond->{last_issue_period_length} );
        }
        if ( $enabled->{last_seen} ) {
            push( @where, "DATEDIFF(now(), items.datelastseen) / ? >= ?" );
            push( @bindParams, $period->{$subcond->{last_seen_period_type}} );
            push( @bindParams, $subcond->{last_seen_period_length} );
        }
        if ( $enabled->{last_acq} ) {
            push( @where, "DATEDIFF(now(), items.dateaccessioned) / ? >= ?" );
            push( @bindParams, $period->{$subcond->{last_acq_period_type}} );
            push( @bindParams, $subcond->{last_acq_period_length} );
        }
        if ( $enabled->{publication_year} ) {
            my $op = '=';
            if ( $subcond->{publication_year_cond} eq 'le' ) {
                $op = '<=';
            }
            elsif ( $subcond->{publication_year_cond} eq 'ge' ) {
                $op = '>=';
            }
            push( @havingParts, '(publicationyear ' . $op . ' ? OR publicationyear = "")' );
            push( @bindParams, $subcond->{publication_year} );
        }



        # retrieve results to display
        my $dbColumns = join(',', @queryCols);
        my $having = (scalar @havingParts > 0) ? "HAVING " . join(' AND ', @havingParts) : '';
        my $subconditions = join(' AND ', @where);
        if ( $subconditions ) {
            $subconditions = " AND $subconditions";
        }
        $query = "SELECT $dbColumns "
            . " FROM items "
            . "LEFT JOIN biblio USING(biblionumber) "
            . "LEFT JOIN biblio_metadata USING(biblionumber) "
            . "LEFT JOIN itemtypes ON itemtypes.itemtype = items.itype "
            . "LEFT JOIN branches ON branches.branchcode = items.holdingbranch "
            . "WHERE withdrawn_on IS NULL $subconditions "
            . "$having "
            . "$limit;";

    if ($returnSql) {
        return ($query, @bindParams);
    }
    else {
        $sth = $dbh->prepare( $query );
        for my $i (0 .. $#bindParams) {
            $sth->bind_param($i + 1, $bindParams[$i]);
        }
        $sth->execute();
    
        return ($sth, @columns);
    }
}

sub tool_show_sql {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId = scalar $cgi->param('predef');

    my ($query, @bindParams) = $self->execute_sql($predefId, 1);

    my $template = $self->get_template({ file => 'tool-sql.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    $query =~ s/(SELECT|FROM|WHERE|LEFT JOIN|IFNULL|TRIM|IS NOT NULL| AND| ON | as | AS | OR)/<strong>$1<\/strong>/g;
    $query =~ s/(FROM|LEFT JOIN|WHERE)/\n$1/g;
    $query =~ s/(\?)/<span style="background-color: #FFFF64; padding: 0 0.5em;"><strong>?<\/strong><\/span>/g;

    $template->param(
        query => $query,
        bind_params => \@bindParams
    );

    print $template->output();
}

sub tool_get_results {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $predefId;

    if ( defined $cgi->param('save') || defined $cgi->param('save_run') ) {
        $predefId = $self->tool_save_predef();
    }

    if ( defined $cgi->param('save') ) {
        $self->tool_list_predefs();
    }
    else {
        my $template = $self->get_template({ file => 'tool-results.tt' });

        print $cgi->header(-type => 'text/html',
                           -charset => 'utf-8');

        if ( !defined $predefId && defined $cgi->param('predef') ) {
            $predefId = $cgi->param('predef');
        }

        my ($sth, @columns) = $self->execute_sql($predefId, 0);

        my @results;
        while ( my $row = $sth->fetchrow_hashref() ) {
            push( @results, $row );
        }

        my %versions = C4::Context::get_versions();
        $template->param(
            columns => \@columns,
            results => \@results,
            rows => scalar @results,
            predef => $predefId,
            version => substr($versions{'kohaVersion'}, 0, 2) . substr($versions{'kohaVersion'}, 3, 2)
        );

        print $template->output();
    }

}

sub tool_save_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $dbh = C4::Context->dbh;
    my $table_predefs = $self->get_qualified_table_name('predefs');
    my $table_options = $self->get_qualified_table_name('predef_options');

    my $predef_id;
    my $query;
    my $sth;

    if ( defined $cgi->param('predef') ) {
        $query = "UPDATE $table_predefs SET name = ?, description = ?, last_modified = now() WHERE predef_id = ?;";

        $predef_id = scalar $cgi->param('predef');

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef,
            $predef_id
        );

        $query = "DELETE FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute($predef_id);
    }
    else {
        $query = "INSERT INTO $table_predefs (name, description, date_created, last_modified) VALUES(?, ?, now(), now());";

        $sth = $dbh->prepare($query);
        $sth->execute(
            defined $cgi->param('predef-name') ? scalar $cgi->param('predef-name') : '(nepojmenováno)',
            defined $cgi->param('predef-descr') ? scalar $cgi->param('predef-descr') : undef
        );

        $predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');
    }


    my @fields = qw(
        limit
        chk_itype itype
        chk_available_items available_items
        chk_item_price item_price
        chk_publication_year publication_year publication_year_cond
        chk_damage damage
        chk_onloan onloan
        chk_issues_max issues_max issues_max_period_length issues_max_period_type
        chk_reserves_max reserves_max reserves_max_period_length reserves_max_period_type
        chk_acqsource acqsource
        chk_branches branches
        chk_locations locations
        chk_ccodes ccodes
        chk_last_issue last_issue_period_length last_issue_period_type
        chk_last_seen last_seen_period_length last_seen_period_type
        chk_last_reserve last_reserve_period_length last_reserve_period_type
        chk_last_acq last_acq_period_length last_acq_period_type

        col_barcode
        col_title
        col_author
        col_itype
        col_callnumber
        col_callnumber_stock
        col_stocknumber
        col_holdingbranch
        col_replacementprice
	col_publicationyear
    );
    my %multi = map { $_ => 1 } qw(
        acqsource
        branches
        locations
        ccodes
    );

    my @data;
    for my $f (@fields) {
        if ( defined $cgi->param($f) ) {
            my $value;
            if ( exists( $multi{$f} ) ) {
                my @options = $cgi->multi_param($f);
                $value = join(',', @options);
            }
            else {
                $value = scalar $cgi->param($f);
            }
            push( @data, ($predef_id, $f, $value) );
        }
    }

    $query = "INSERT INTO $table_options (predef_id, variable, value) VALUES ";
    $query .= "(?, ?, ?)," x (scalar @data / 3);
    $query =~ s/,$/;/g;

    $sth = $dbh->prepare($query);
    $sth->execute( @data );

    return $predef_id;
}

sub tool_delete_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');

        my $query = "DELETE FROM $table_predefs WHERE predef_id = ?;";

        my $sth = $dbh->prepare($query);
        $sth->execute( scalar $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

sub tool_duplicate_predef {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    if ( defined $cgi->param('predef') ) {
        my $dbh = C4::Context->dbh;
        my $table_predefs = $self->get_qualified_table_name('predefs');
        my $table_options = $self->get_qualified_table_name('predef_options');

        my $query = "INSERT INTO $table_predefs (name, description, date_created, last_modified) SELECT CONCAT('(kopie ', ? ') ', name), description, now(), now() FROM $table_predefs WHERE predef_id = ?;";
        my $sth = $dbh->prepare($query);
        $sth->execute( $cgi->param('predef'), $cgi->param('predef') );
        my $new_predef_id = $dbh->last_insert_id(undef, undef, $table_predefs, 'predef_id');

        $query = "INSERT INTO $table_options (predef_id, variable, value) SELECT ?, variable, value FROM $table_options WHERE predef_id = ?;";
        $sth = $dbh->prepare($query);
        $sth->execute( $new_predef_id, $cgi->param('predef') );
    }

    $self->tool_list_predefs();
}

# pass $sth, get back an array of names for the column headers
sub header_cell_values {
    my $sth = shift or return ();
    return '' unless ($sth->{NAME});
    my @dbHeaders = @{$sth->{NAME}};
    my @headers;

    foreach my $hdr (@dbHeaders) {
        push( @headers, 'Čárový kód' )          if ( $hdr eq 'barcode' );
        push( @headers, 'Odkaz na titul' )      if ( $hdr eq 'title' );
        push( @headers, 'Název titulu' )        if ( $hdr eq 'title_text' );
        push( @headers, 'Autor' )               if ( $hdr eq 'author' );
        push( @headers, 'Typ jednotky' )        if ( $hdr eq 'itemtype' );
        push( @headers, 'Signatura sv.' )       if ( $hdr eq 'itemcallnumber' );
        push( @headers, 'Signatura skl.' )      if ( $hdr eq 'itemcallnumber_stock' );
        push( @headers, 'Přírůstkové č.' )      if ( $hdr eq 'stocknumber' );
        push( @headers, 'Aktuální umístění' )   if ( $hdr eq 'holdingbranch' );
        push( @headers, 'Nákupní cena' )        if ( $hdr eq 'replacementprice' );
        push( @headers, 'Rok vydání' )          if ( $hdr eq 'publicationyear' );
    }
    return @headers;
}

#sub tool_export_results {
#    my ( $self, $args ) = @_;
#    my $cgi = $self->{'cgi'};
#    my $format = $cgi->param('format');
#
#    my ($sth, @columns) = $self->execute_sql(scalar $cgi->param('predef'));
#    my ( $type, $content );
#
#    if ($format eq 'tab') {
#        $type = 'application/octet-stream';
#        $content .= join("\t", header_cell_values($sth)) . "\n";
#        #$content = Encode::decode('UTF-8', $content);
#        while (my $row = $sth->fetchrow_arrayref()) {
#            $content .= join("\t", @$row) . "\n";
#        }
#    }
#    elsif ( $format eq 'csv' ) {
#        my $delimiter = C4::Context->preference('delimiter') || ',';
#        $delimiter = "\t" if $delimiter eq 'tabulation';
#        $type = 'application/csv';
#        my $csv = Text::CSV::Encoded->new({ encoding_out => 'UTF-8', sep_char => $delimiter});
#        $csv or die "Text::CSV::Encoded->new({binary => 1}) FAILED: " . Text::CSV::Encoded->error_diag();
#        if ($csv->combine(header_cell_values($sth))) {
#            $content .= Encode::decode('UTF-8', $csv->string()) . "\n";
#        }
#        while (my $row = $sth->fetchrow_arrayref()) {
#            if ($csv->combine(@$row)) {
#                $content .= $csv->string() . "\n";
#            }
#        }
#    }
#    elsif ( $format eq 'ods' ) {
#        $type = 'application/vnd.oasis.opendocument.spreadsheet';
#        my $ods_fh = File::Temp->new( UNLINK => 0 );
#        my $ods_filepath = $ods_fh->filename;
#
#        use OpenOffice::OODoc;
#        my $tmpdir = dirname $ods_filepath;
#        odfWorkingDirectory( $tmpdir );
#        my $container = odfContainer( $ods_filepath, create => 'spreadsheet' );
#        my $doc = odfDocument (
#            container => $container,
#            part      => 'content'
#        );
#        my $table = $doc->getTable(0);
#        my @headers = header_cell_values( $sth );
#        my $rows = $sth->fetchall_arrayref();
#        my ( $nb_rows, $nb_cols ) = ( 0, 0 );
#        $nb_rows = @$rows;
#        $nb_cols = @headers;
#        $doc->expandTable( $table, $nb_rows + 1, $nb_cols );
#
#        my $row = $doc->getRow( $table, 0 );
#        my $j = 0;
#        for my $header ( @headers ) {
#            my $value = Encode::encode( 'UTF8', $header );
#            $doc->cellValue( $row, $j, $value );
#            $j++;
#        }
#        my $i = 1;
#        for ( @$rows ) {
#            $row = $doc->getRow( $table, $i );
#            for ( my $j = 0 ; $j < $nb_cols ; $j++ ) {
#                my $value = Encode::encode( 'UTF8', $rows->[$i - 1][$j] );
#                $doc->cellValue( $row, $j, $value );
#            }
#            $i++;
#        }
#        $doc->save();
#        binmode(STDOUT);
#        open $ods_fh, '<', $ods_filepath;
#        $content .= $_ while <$ods_fh>;
#        unlink $ods_filepath;
#    }
#
#    print $cgi->header(
#        -type => $type,
#        -attachment=> 'odpisy.' . $format
#    );
#    print $content;
#
#    exit;
#}

sub tool_withdraw_select {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    my $template = $self->get_template({ file => 'withdrawals.tt' });

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    print $template->output();
}

sub tool_withdraw_show {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $dbh = C4::Context->dbh;

    my @itemnumbers;
    my $biblionumber;
	my $withdrawal   = $cgi->param('withdrawal');
	my $withdrawn_categorycode   = $cgi->param('withdrawn_categorycode');

    require C4::Auth;                                                          
                                                                               
    # if not absolute, call mbf_path, which dies if file does not exist        
    my $template_name = $self->mbf_path( 'withdrawals-show.tt' );                         
    my ( $template, $loggedinuser, $cookie ) = C4::Auth::get_template_and_user(
        {   template_name   => $template_name,                                 
            query           => $self->{'cgi'},                                 
            type            => "intranet",                                     
            authnotrequired => 1,                                              
        }                                                                      
    );                                                                         

    $template->param(
        CLASS       => $self->{'class'},
        METHOD      => scalar $self->{'cgi'}->param('method'),
        PLUGIN_PATH => $self->get_plugin_http_path(),
        PLUGIN_DIR  => $self->bundle_path(),
    );

	# Does the user have a restricted item edition permission?
	my $uid = $loggedinuser ? Koha::Patrons->find( $loggedinuser )->userid : undef;
	my $restrictededition = $uid ? C4::Auth::haspermission($uid,  {'tools' => 'items_batchmod_restricted'}) : undef;
	# In case user is a superlibrarian, edition is not restricted
	$restrictededition = 0 if ($restrictededition != 0 && C4::Context->IsSuperLibrarian());
 
	$template->param(withdrawal=> $withdrawal);
	$template->param(withdrawn_categorycode => $withdrawn_categorycode);

	my @errors; # store errors found while checking data BEFORE saving item.
	my $items_display_hashref;
	#our $tagslib = &GetMarcStructure(1);

    my $filefh = $cgi->upload('uploadfile');
    my $filecontent = $cgi->param('filecontent');
    my ( @notfoundbarcodes, @notfounditemnumbers);

    my @contentlist;
    if ($filefh){
        binmode $filefh, ':encoding(UTF-8)';
        while (my $content=<$filefh>){
            $content =~ s/[\r\n]*$//;
            push @contentlist, $content if $content;
        }

        @contentlist = uniq @contentlist;
        if ($filecontent eq 'barcode_file') {
            my $existing_items = Koha::Items->search({ barcode => \@contentlist });
            @itemnumbers = $existing_items->get_column('itemnumber');
            my %exists = map {lc($_)=>1} $existing_items->get_column('barcode');
            # to avoid problems with case sensitivity
            foreach my $barcode (@contentlist) {
                $barcode = lc($barcode);
            }
            @notfoundbarcodes = grep { !$exists{$_} } @contentlist;
        }
        elsif ( $filecontent eq 'itemid_file') {
            @itemnumbers = Koha::Items->search({ itemnumber => \@contentlist })->get_column('itemnumber');
            my %exists = map {$_=>1} @itemnumbers;
            @notfounditemnumbers = grep { !$exists{$_} } @contentlist;
        }
    } else {
        if (defined $biblionumber && !@itemnumbers){
            my @all_items = GetItemsInfo( $biblionumber );
            foreach my $itm (@all_items) {
                push @itemnumbers, $itm->{itemnumber};
            }
        }
        if ( my $list=$cgi->param('barcodelist')){
        	$list =~ s/[ \t]*//gm; # remove spaces
        	
        	my @barcodelist;
        	foreach my $barcode ( uniq( split(/\s\n/, $list) ) ) {
        		push @barcodelist, $barcode if ( $barcode ne '' );
        	}

            my $existing_items = Koha::Items->search({ barcode => \@barcodelist });
            @itemnumbers = $existing_items->get_column('itemnumber');
            my @barcodes = $existing_items->get_column('barcode');
            my %exists = map {lc($_)=>1} @barcodes;
            # to avoid problems with case sensitivity
            foreach my $barcode (@barcodelist) {
                $barcode = lc($barcode);
            }
            @notfoundbarcodes = grep { !$exists{$_} } @barcodelist;
        }
    }

    # Flag to tell the template there are valid results, hidden or not
    if (scalar(@itemnumbers) > 0) { $template->param(itemresults => 1); }
    # Only display the items if there are no more than pref MaxItemsToProcessForBatchMod or MaxItemsToDisplayForBatchDel
    my $max_display_items = C4::Context->preference("MaxItemsToDisplayForBatchMod");
    $template->param("too_many_items_process" => scalar(@itemnumbers)) if scalar(@itemnumbers) >= C4::Context->preference("MaxItemsToProcessForBatchMod");
    if (scalar(@itemnumbers) <= ( $max_display_items // 1000 ) ) {
        $items_display_hashref=$self->buildItemsData(@itemnumbers);
    } else {
        $template->param("too_many_items_display" => scalar(@itemnumbers));
        # Even if we do not display the items, we need the itemnumbers
        $template->param(itemnumbers_array => \@itemnumbers);
    }

    my %versions = C4::Context::get_versions();
    $template->param(
        #item                => \@loop_data,
        notfoundbarcodes    => \@notfoundbarcodes,
        notfounditemnumbers => \@notfounditemnumbers,
        show => 1,
        version => substr($versions{'kohaVersion'}, 0, 2) . substr($versions{'kohaVersion'}, 3, 2)
    );
    $template->param(%$items_display_hashref) if $items_display_hashref;

	foreach my $error (@errors) {
	    $template->param($error => 1) if $error;
	}

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    print $template->output();
}

sub buildItemsData{
    my ( $self, @itemnumbers ) = @_;
    
#    die(Dumper(\@itemnumbers));

    # now, build existing item list
    my %witness; #---- stores the list of subfields used at least once, with the "meaning" of the code
    my @big_array;

    #---- finds where items.itemnumber is stored
    my (  $itemtagfield,   $itemtagsubfield) = &C4::Biblio::GetMarcFromKohaField("items.itemnumber", "");
    my ($branchtagfield, $branchtagsubfield) = &C4::Biblio::GetMarcFromKohaField("items.homebranch", "");

    foreach my $itemnumber (@itemnumbers){
        my $itemdata = Koha::Items->find($itemnumber);
        next unless $itemdata; # Should have been tested earlier, but just in case...
        $itemdata = $itemdata->unblessed;
        my $itemmarc=C4::Items::Item2Marc($itemdata);
        my %this_row;
        foreach my $field (grep {$_->tag() eq $itemtagfield} $itemmarc->fields()) {
            # loop through each subfield
            my $itembranchcode=$field->subfield($branchtagsubfield);
            if ($itembranchcode && C4::Context->preference("IndependentBranches")) {
                #verifying rights
                my $userenv = C4::Context->userenv();
                unless (C4::Context->IsSuperLibrarian() or (($userenv->{'branch'} eq $itembranchcode))){
                    $this_row{'nomod'} = 1;
                }
            }
            my $tag=$field->tag();
            foreach my $subfield ($field->subfields) {
                my ($subfcode,$subfvalue)=@$subfield;
                next if ($tagslib->{$tag}->{$subfcode}->{tab} ne 10 
                        && $tag        ne $itemtagfield 
                        && $subfcode   ne $itemtagsubfield);

                $witness{$subfcode} = $tagslib->{$tag}->{$subfcode}->{lib} if ($tagslib->{$tag}->{$subfcode}->{tab}  eq 10);
                if ($tagslib->{$tag}->{$subfcode}->{tab}  eq 10) {
                    $this_row{$subfcode} = C4::Biblio::GetAuthorisedValueDesc( $tag, $subfcode, $subfvalue, '', $tagslib) || $subfvalue;
                }

                $this_row{itemnumber} = $subfvalue if ($tag eq $itemtagfield && $subfcode eq $itemtagsubfield);
            }
        }

        # grab title, author, and ISBN to identify bib that the item
        # belongs to in the display
        my $biblio = Koha::Biblios->find( $itemdata->{biblionumber} );
        $this_row{title}        = $biblio->title;
        $this_row{author}       = $biblio->author;
        $this_row{isbn}         = $biblio->biblioitem->isbn;
        $this_row{biblionumber} = $biblio->biblionumber;
        $this_row{holds}        = $biblio->holds->count;
        $this_row{item_holds}   = Koha::Holds->search( { itemnumber => $itemnumber } )->count;
        $this_row{itype}        = $itemdata->{itype};
        $this_row{withdrawn}    = $itemdata->{withdrawn};

        if (%this_row) {
            push(@big_array, \%this_row);
        }
    }
    @big_array = sort {$a->{0} cmp $b->{0}} @big_array;

    my @serial_itemtypes = split(/[,\s]+/, $self->retrieve_data('serial_itemtypes'));

    # now, construct template !
    # First, the existing items for display
    my @item_value_loop;
    my @witnesscodessorted=sort keys %witness;
    my %lost_flags;
    for my $row ( @big_array ) {
        my %row_data;
        my @item_fields = map +{ field => $_ || '' }, @$row{ @witnesscodessorted };

        if ( defined $row->{b} && "$row->{b}" ne '0' ) {
        	$lost_flags{ $row->{b} } = 1;
        }

        $row_data{item_value} = [ @item_fields ];
        $row_data{itemnumber} = $row->{itemnumber};
        
        #reporting this_row values
        $row_data{nomod} = $row->{nomod};
        $row_data{bibinfo} = $row->{bibinfo};
        $row_data{author} = $row->{author};
        $row_data{title} = $row->{title};
        $row_data{isbn} = $row->{isbn};
        $row_data{biblionumber} = $row->{biblionumber};
        $row_data{holds}        = $row->{holds};
        $row_data{item_holds}   = $row->{item_holds};

        my $is_on_loan = C4::Circulation::IsItemIssued( $row->{itemnumber} );
        $row_data{onloan} = $is_on_loan ? 1 : 0;
        
	    my $collection_tracking = Koha::Plugin::Com::RBitTechnology::SmartWithdrawals::Trackings->find( { itemnumber => $row->{itemnumber} } );
        $row_data{is_rotating} = defined $collection_tracking ? 1 : 0;
        
        my $itemtype = $row->{itype};
        $row_data{serial_item} = grep( /^$itemtype$/, @serial_itemtypes ) ? 1 : 0; 

        $row_data{already_withdrawn} = $row->{withdrawn} ? 1 : 0;

        push(@item_value_loop,\%row_data);
    }
    my @header_loop=map { { header_code => $_, header_value=> $witness{$_} } } @witnesscodessorted;

    return {
        item_loop => \@item_value_loop,
        item_header_loop => \@header_loop,
        lost_flags => join(', ', ( keys %lost_flags ) )
    };
}

sub get_max_withdrawal {
    my ( $self, $itemdata ) = @_;
    my $dbh = C4::Context->dbh;

    my $date_cond = '';
    my $start_date = '';
    if ( $self->retrieve_data('start_date') > 0 ) {
        $date_cond = ' AND DATE(withdrawn_on) >= ?';
        $start_date = output_pref({ dt => dt_from_string( $self->retrieve_data('start_date')), dateformat => 'iso', dateonly => 1 });
    }

    my $sql="SELECT MAX(CAST( withdrawn_permanent AS UNSIGNED INT)) AS max
            FROM items
            WHERE withdrawn_permanent IS NOT NULL $date_cond";

    my @params;
    if ($self->retrieve_data('start_date') > 0) {
        push @params, $start_date;
    }
    
    my @columns = split('\|', $self->retrieve_data('permanent_withdrawn_number_items_columns') // '');
    foreach my $column ( @columns ) {
        $sql = $sql." AND ".$column." = ?";
        push @params, $itemdata->$column;
    }
    my $sth = $dbh->prepare($sql);
    $sth->execute(@params);
    
    my $row = $sth->fetchrow_hashref();
    my $max;
    if (!$row || !defined $row->{'max'}) {
        $max = $self->retrieve_data('permanent_withdrawn_seed') // 0;
    }
    else {
        $max = $row->{'max'};
    }
    
    return $max;
}

sub adjust_db_schema {
    my ( $self, $args ) = @_;
	
    # this is a HACK of Koha database schema, because it knows nothing about czech withdrawal rules
    my $rs = Koha::Database->new->schema->resultset('Item');
    $rs->result_source->add_columns("withdrawn_permanent");
    $rs->result_source->add_columns("withdrawn_categorycode");
    Koha::Schema::Result::Item->add_columns(
        "withdrawn_permanent",
        {data_type=>"varchar",is_nullable=>1,size=>32},
        "withdrawn_categorycode",
        {data_type=>"varchar",is_nullable=>1,size=>10}
    );
    Koha::Schema::Result::Item->register_column("withdrawn_permanent");
    Koha::Schema::Result::Item->register_column("withdrawn_categorycode");
}

sub tool_withdraw_action {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};
    my $dbh = C4::Context->dbh;

    my @itemnumbers = $cgi->multi_param('itemnumber');
    my $biblionumber;
    my $withdrawal = $cgi->param('withdrawal');
    my $withdrawn_categorycode = $cgi->param('withdrawn_categorycode');
    my $clear_lost = defined $cgi->param('clear_lost') && $cgi->param('clear_lost') ? 1 : 0;
	my $completedJobID = $cgi->param('completedJobID');
	my $runinbackground = $cgi->param('runinbackground');
    my $withdrawn_items = 0;     # Number of withdrawned items
    my $items_display_hashref;

    require C4::Auth;                                                          
                                                                               
    # if not absolute, call mbf_path, which dies if file does not exist        
    my $template_name = $self->mbf_path( 'withdrawals-result.tt' );                         
    my ( $template, $loggedinuser, $cookie ) = C4::Auth::get_template_and_user(
        {   template_name   => $template_name,                                 
            query           => $self->{'cgi'},                                 
            type            => "intranet",                                     
            authnotrequired => 1,                                              
        }                                                                      
    );                                                                         

    $template->param(
        CLASS       => $self->{'class'},
        METHOD      => scalar $self->{'cgi'}->param('method'),
        PLUGIN_PATH => $self->get_plugin_http_path(),
        PLUGIN_DIR  => $self->bundle_path(),
    );

    # Does the user have a restricted item edition permission?
    my $uid = $loggedinuser ? Koha::Patrons->find( $loggedinuser )->userid : undef;
    my $restrictededition = $uid ? C4::Auth::haspermission($uid,  {'tools' => 'items_batchmod_restricted'}) : undef;
    # In case user is a superlibrarian, edition is not restricted
    $restrictededition = 0 if ($restrictededition != 0 && C4::Context->IsSuperLibrarian());

	my %cookies = CGI::Cookie->fetch();
	my $sessionID = $cookies{'CGISESSID'}->value;

    my @errors; # store errors found while checking data BEFORE saving item.

    # Once the job is done
    if ($completedJobID) { #die("completed");
	    # If we have a reasonable amount of items, we display them
	    my $max_items = C4::Context->preference("MaxItemsToDisplayForBatchMod");
	    if (scalar(@itemnumbers) <= $max_items ){
	        if (scalar(@itemnumbers) <= 1000 ) {
	            $items_display_hashref = $self->buildItemsData(@itemnumbers);
	        } else {
	            # Else, we only display the barcode
	            my @simple_items_display = map {
	                my $itemnumber = $_;
	                my $item = Koha::Items->find($itemnumber);
	                {
	                    itemnumber   => $itemnumber,
	                    barcode      => $item ? ( $item->barcode // q{} ) : q{},
	                    biblionumber => $item ? $item->biblio->biblionumber : q{},
	                };
	            } @itemnumbers;
	            $template->param("simple_items_display" => \@simple_items_display);
	        }
	    } else {
	        $template->param( "too_many_items_display" => scalar(@itemnumbers) );
	        $template->param( "job_completed" => 1 );
	    }
	
	    # Setting the job as done
	    my $job = C4::BackgroundJob->fetch($sessionID, $completedJobID);
	
	    # Calling the template
	    add_saved_job_results_to_template($template, $completedJobID);
	
    } else {
	    # While the job is getting done

	    # Job size is the number of items we have to process
	    my $job_size = scalar(@itemnumbers);
	    my $job = undef;
	
	    # If we asked for background processing
	    if ($runinbackground) {
	        $job = put_in_background($job_size);
	    }

        $self->adjust_db_schema;
        
	    # For each item
	    my $i = 1; 
	    foreach my $itemnumber(@itemnumbers){
	
	        $job->progress($i) if $runinbackground;
	        my $itemdata = Koha::Items->find($itemnumber);
	        next unless $itemdata; # Should have been tested earlier, but just in case...

            my $max = $self->get_max_withdrawal($itemdata);

            $itemdata->set(
                {
                	withdrawn => 1,
                	withdrawn_permanent => ++$max,
                	withdrawn_categorycode => $withdrawn_categorycode,
                	itemlost => $clear_lost ? 0 : ( defined $itemdata->itemlost ? $itemdata->itemlost : 0 ) 
                }
            );
			$itemdata->store;

		    my $indexer = Koha::SearchEngine::Indexer->new({ index => $Koha::SearchEngine::BIBLIOS_INDEX });
		    $indexer->index_records( $itemdata->{biblionumber}, "specialUpdate", "biblioserver" );
			
			$withdrawn_items++;
	     
            $i++;
	    }
    }
    
    $template->param( 
        withdrawn_items => $withdrawn_items, 
        action => 1 
    );

    foreach my $error (@errors) {
        $template->param($error => 1) if $error;
    }

    print $cgi->header(-type => 'text/html',
                       -charset => 'utf-8');

    print $template->output();
}

sub ajax_test {
    my ( $self ) = @_;

    my $cgi = $self->{'cgi'};
    print $cgi->header(-type => 'text/json',
                       -charset => 'utf-8');

    my $response = {};
    
    if ( $cgi->param('barcode') eq '' ) {
        $response->{result} = 'no-barcode'; 
    }
    else {
	    $self->adjust_db_schema;
	    my $itemdata = Koha::Items->find( { barcode => $cgi->param('barcode') } );
	    if ( $itemdata ) {
	        my $max = $self->get_max_withdrawal($itemdata);
	        $response->{result} = 'OK'; 
	        $response->{max} = ++$max; 
	    }
	    else {
	        $response->{result} = 'item-not-found'; 
	    }
    }
    
    print to_json( $response );
}

1;
