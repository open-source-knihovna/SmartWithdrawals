$(document).ready(function () {

  /* úprava odkazů na dávkové odpisy */
  var link = $("dt a[href='/cgi-bin/koha/tools/batchMod.pl?withdrawal=1']");
  
  if (link.length) {
    link.attr('href', '/cgi-bin/koha/plugins/run.pl?class=Koha::Plugin::Com::RBitTechnology::SmartWithdrawals&method=tool&phase=withdraw-select');
  } 
  else {
    $("dt a[href='/cgi-bin/koha/tools/batchMod.pl']").before('<dt><a href="/cgi-bin/koha/plugins/run.pl?class=Koha::Plugin::Com::RBitTechnology::SmartWithdrawals&amp;method=tool&amp;phase=withdraw-select">Dávkový odpis jednotek</a></dt><dd>Hromadné odepisování jednotek</dd>');
  }

  /* úprava odkazů na dávkové odpisy */

});
